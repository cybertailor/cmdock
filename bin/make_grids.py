#!/usr/bin/python3
# -*- coding: utf-8 -*-
# CmDock - cmcalcgrid
# v mj0.1
# 
# go


import os, sys

#if you dont have the env set, modifyl next line
cmdock_root = str(os.getenv('CMDOCK_ROOT'))+str("\\build\cmcalcgrid")

#default settings for cmcalcgrid
set_gridstep = 0.3
set_border = 1.0

#check argvs
if len(sys.argv) <=1:
    print ("-"*40)
    print ("Please add grid files as script arguments in command line.")
    print ("cmcalcgrid will calculate grids for all receptors defined in script arguments")
    print ("-"*40)
    quit()
else:
    pass

#go cmcalcgrid
if os.name == 'nt':
    print ("Win system")
    print ("CMDOCK_ROOT location: " + str(cmdock_root))
    for i in sys.argv[1:]:
        print ("go: "+ str(i))
        os.popen("{} -r{} -pcalcgrid_vdw1.prm -o_wdw1.grd -g{} -b{}".format(cmdock_root, i, set_gridstep, set_border))
        os.popen("{} -r{} -pcalcgrid_vdw5.prm -o_wdw5.grd -g{} -b{}".format(cmdock_root, i, set_gridstep, set_border))
elif os.name =='posix':
    print ("*nix system")
    print ("CMDOCK_ROOT: " + str(cmdock_root))
    for i in sys.argv[1:]:
        print ("go: "+ str(i))
        os.system("{} -r{} -pcalcgrid_vdw1.prm -o_wdw1.grd -g{} -b{}".format(cmdock_root, i, set_gridstep, set_border))
        os.system("{} -r{} -pcalcgrid_vdw5.prm -o_wdw5.grd -g{} -b{}".format(cmdock_root, i, set_gridstep, set_border))
else:
    print ("Please use Win or *nix system")
    quit()
	


