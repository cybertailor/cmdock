
#include <iostream>
#include <fstream>
#include <cstring>
#include <RbtCompression.hpp>
#include <bundle/bundle.h>
#include <bundle/bundle.cpp>

#define eprint(err) std::cerr << err;
#define eprintln(err) std::cerr << err << std::endl;

bool is_ascii(const signed char *c, size_t len) {
  for (size_t i = 0; i < len; i++) {
    if (c[i] < 0) return false;
  }
  return true;
}


#ifndef DOXYGEN_SHOULD_SKIP_THIS
// Converts char[8] (little endian) to uint64_t
// TODO: Figure out why this sometimes fails?
uint64_t LE_Bytes::from(const char* bytes) {
    return
        (uint64_t(bytes[7]) << 8*7) |
        (uint64_t(bytes[6]) << 8*6) |
        (uint64_t(bytes[5]) << 8*5) |
        (uint64_t(bytes[4]) << 8*4) |
        (uint64_t(bytes[3]) << 8*3) |
        (uint64_t(bytes[2]) << 8*2) |
        (uint64_t(bytes[1]) << 8*1) |
        (uint64_t(bytes[0]) << 8*0);
}

// Convert uint64_t to char[8] (little-endian)
void LE_Bytes::to(uint64_t uint64, char* destBuf) {
    char bytes[8];
    bytes[7] = char(uint64 >> 8*7);
    bytes[6] = char(uint64 >> 8*6);
    bytes[5] = char(uint64 >> 8*5);
    bytes[4] = char(uint64 >> 8*4);
    bytes[3] = char(uint64 >> 8*3);
    bytes[2] = char(uint64 >> 8*2);
    bytes[1] = char(uint64 >> 8*1);
    bytes[0] = char(uint64 >> 8*0);

    *(uint64_t*)destBuf = *(uint64_t*)bytes;
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

// Read compressed index from inputFile and store in compressedIndex
bool CmZ::archive::readCompressedIndex() {
    // Save position in file
    uint64_t currentPos = inputFile.tellg();

    try {
        // Find length of file
        inputFile.seekg(0, std::ios_base::end);
        uint64_t fileSize = inputFile.tellg();

        // Read index length
        inputFile.seekg(fileSize - 8);
        char* indexSizeRaw = new char[8];
        inputFile.read(indexSizeRaw, 8);
        
        uint64_t indexSize;
        std::memmove(&indexSize, indexSizeRaw, sizeof(indexSizeRaw));
        //TODO: uint64_t indexSize = LE_Bytes::from(indexSizeRaw);
        
        // Read data into compressedIndex
        inputFile.seekg(fileSize - indexSize - 8);
        compressedIndex.resize(indexSize);
        inputFile.read(&compressedIndex[0], indexSize);
        
        // Seek to saved position
        inputFile.seekg(currentPos);

        return true;
    } catch (const std::exception &exc) {
        inputFile.seekg(currentPos);
        eprintln(exc.what());
        return false;
    }
}

// Decompress data in compressedIndex, parse, then store it in index
bool CmZ::archive::decompressIndex() {
    try {
        std::vector<char> indexRaw;
        indexRaw = bundle::unpack(compressedIndex); // Decompress to get size
        index.resize(indexRaw.size() / 8);
        std::memmove(index.data(), indexRaw.data(), indexRaw.size());
        
        /* TODO: Make this portable to even big endian systems
        char* buffer = new char[8];
        int i = 0;
        int j = 0;
        for (char byte : indexRaw) {
            buffer[i] = byte;
            i ++;
            if (i == 8) {
                index[j] = LE_Bytes::from(buffer);
                j ++;
                i = 0;
            }
        }*/

        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Convert index to std::vector<char> (little endian) then compress into compressedIndex
bool CmZ::archive::compressIndex() {
    try {
        uint64_t i = 0;
        std::vector<char> indexRaw;
        indexRaw.resize(index.size() * 8); // Allocate memory
        for (uint64_t ind : index) { 
            char* buffer = new char[8];
            LE_Bytes::to(ind, buffer); // Convert uint64_t to char[8] 
            for (int j = 0; j < 8; j ++) {
                indexRaw[i] = buffer[j]; // Append data to temp index
                i ++;
            }
        }
        compressedIndex = bundle::pack(mode, indexRaw); // Compress index
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

uint64_t CmZ::archive::calculateOffset(uint64_t ind) {
    uint64_t offset = 0;
    if (ind == UINT64_MAX) {
        ind = index.size() - 1;
    }

    for (size_t i = 0; i <= ind; i ++) {
        offset += index[i];
    }

    return offset;
}

// Clear compressedRecord and read new compressed record data from file 
bool CmZ::archive::readRecordAtIndex(uint64_t ind) {
    if (ind == UINT64_MAX) {
        ind = index.size() - 2;
    }
    try {
        // Calculate record's offset from index
        uint64_t offset = calculateOffset(ind);
        uint64_t size = index[ind + 1]; // Read record's size from index

        // Read data into compressedRecord
        inputFile.seekg(offset);
        compressedRecord.resize(size);
        inputFile.read(&compressedRecord[0], size);

        nextRecord = ind + 1;

        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }

}

bool CmZ::archive::readNextRecord() {
    try {
        readRecordAtIndex(nextRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Compress decompressedRecord and store in compressedRecord
bool CmZ::archive::compress() {
    try {
        compressedRecord = bundle::pack(mode, decompressedRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Decompress compressedRecord and store in decompressedRecord
bool CmZ::archive::decompress() {
    try {
        decompressedRecord = bundle::unpack(compressedRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Calculate index entry and append to index
bool CmZ::archive::addCurrentToIndex() {
    try {
        if (index.size() == 0) 
            index.push_back(0);
        index.push_back(compressedRecord.size());
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Write to file from appropriate buffer
bool CmZ::archive::writeRecordToFile() {
    try {
        if (mode == DECOMPRESS) {
            outputFile.write((char*) decompressedRecord.data(), decompressedRecord.size());
        } else {
            outputFile.write((char*) compressedRecord.data(), compressedRecord.size());
        }
        outputFile.sync();
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile) {
    std::vector<uint64_t> index = {};
    std::ifstream input(strInputFile, std::ios_base::binary);
    uint64_t pos = 0;
    uint64_t recordStart = 0;
    char *header = new char[32];
    unsigned iAlgorithm;
    while (input) {
        char currentByteBuf[5];
        input.seekg(pos);
        input.read(currentByteBuf, 5);
        if (currentByteBuf[0] == 0x00 && currentByteBuf[1] == 0x00 && currentByteBuf[2] == 0x00) {
            if (currentByteBuf[3] == 0x70) {
                if ((unsigned int)currentByteBuf[4] < 24) { // Found header
                    if (index.size() == 0)
                        index.push_back(recordStart);
                    input.seekg(recordStart);
                    input.read(header, 32);
                    size_t pad = bundle::padding(header, 32);
                    iAlgorithm = header[pad + 1];
                    const char *ptr = (const char *)&header[pad + 2];
                    size_t unpackedSize = bundle::vlebit(ptr);
                    size_t packedSize = bundle::vlebit(ptr);
                    index.push_back(packedSize + 32);
                    pos = recordStart + packedSize + 32;
                    recordStart = pos;
                    continue;
                }
            }
        }
        pos++;
        if (currentByteBuf[0] != 0x00) {
            recordStart = pos;
        }
    }
    input.close();
    return index;
}

#ifdef _WIN32
std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile, indicators::ProgressBar &progressBar) {
#else
std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile, indicators::BlockProgressBar &progressBar) {
#endif
    std::vector<uint64_t> index = {};
    std::ifstream input(strInputFile, std::ios_base::binary);
    input.seekg(0, std::ios_base::end);
    uint64_t size = input.tellg();
    input.seekg(0, std::ios_base::beg);
    uint64_t pos = 0;
    uint64_t recordStart = 0;
    char *header = new char[32];
    unsigned iAlgorithm;
    while (input) {
        char currentByteBuf[5];
        input.seekg(pos);
        input.read(currentByteBuf, 5);
        if (currentByteBuf[0] == 0x00 && currentByteBuf[1] == 0x00 && currentByteBuf[2] == 0x00) {
            if (currentByteBuf[3] == 0x70) {
                if ((unsigned int)currentByteBuf[4] < 24) { // Found header
                    if (index.size() == 0)
                        index.push_back(recordStart);
                    input.seekg(recordStart);
                    input.read(header, 32);
                    size_t pad = bundle::padding(header, 32);
                    iAlgorithm = header[pad + 1];
                    const char *ptr = (const char *)&header[pad + 2];
                    size_t unpackedSize = bundle::vlebit(ptr);
                    size_t packedSize = bundle::vlebit(ptr);
                    index.push_back(packedSize + 32);
                    pos = recordStart + packedSize + 32;
                    recordStart = pos;
                    double progress = static_cast<double>(pos) / static_cast<double>(size);
                    progressBar.set_option(indicators::option::PostfixText{"Headers found: " + std::to_string(index.size() - 2)});
                    progressBar.set_progress(progress * 100);
                    continue;
                }
            }
        }
        pos++;
        if (currentByteBuf[0] != 0x00) {
            recordStart = pos;
        }
        double progress = static_cast<double>(pos) / static_cast<double>(size);
        progressBar.set_progress(progress * 100);
    }
    input.close();
    return index;
}

bool CmZ::isValidMdlHeader(std::vector<char> input) {
    std::vector<char>::iterator offset = input.begin();
    std::uint64_t readLen = input.size() - 1;

    // Header should have at least 3 lines
    if (std::count(offset, offset + readLen, '\n') < 3) {
        return false;
    }

    // Find location of 3rd new line
    for (int i = 0 ; i < 3; i ++) {
        offset = std::find(offset, offset + readLen, '\n');
        readLen = readLen - std::distance(input.begin(), offset);
    }

    int size = std::distance(input.begin(), offset);
    signed char *header = new signed char[size];
    std::memmove(header, input.data(), size);

    if (is_ascii(header, size)) {
        return true;
    }

    return false;
}

#ifdef BUNDLE_NO_SSE
void CmZ::printSupportedAlgorithms() {
    std::cout << "  0 - RAW\t 5 - LZ4\t10 - BROTLI11\t15 - ZMOLLY" << std::endl;
    std::cout << "  1 - LZ4F\t 6 - BROTLI9\t11 - SHRINKER\t16 - CRUSH" << std::endl;
    std::cout << "  2 - MINIZ\t 7 - ZSTD\t12 - ZSTDF\t17 - LZJB" << std::endl;
    std::cout << "  3 - LZIP\t 8 - LZMA25\t13 - BCM\t18 - BZIP2" << std::endl;
    std::cout << "  4 - LZMA20\t 9 - BSC  \t14 - ZLING" << std::endl;
}
#else
void CmZ::printSupportedAlgorithms() {
    std::cout << "Supported compression algorithms:" << std::endl;
    std::cout << "  0 - RAW\t 6 - ZPAQ\t12 - BROTLI11\t18 - MCM" << std::endl;
    std::cout << "  1 - SHOCO\t 7 - LZ4\t13 - SHRINKER\t19 - TANGELO" << std::endl;
    std::cout << "  2 - LZ4F\t 8 - BROTLI9\t14 - CSC20\t20 - ZMOLLY" << std::endl;
    std::cout << "  3 - MINIZ\t 9 - ZSTD\t15 - ZSTDF\t21 - CRUSH" << std::endl;
    std::cout << "  4 - LZIP\t10 - LZMA25\t16 - BCM\t22 - LZJB" << std::endl;
    std::cout << "  5 - LZMA20\t11 - BSC\t17 - ZLING\t23 - BZIP2" << std::endl;
}
#endif

const std::vector<std::string> encoderNames { 
    "RAW", "SHOCO", "LZ4F", "MINIZ", "LZIP", "LZMA20", "ZPAQ", "LZ4",
    "BROTLI9", "ZSTD", "LZMA25", "BSC", "BROTLI11", "SHRINKER", "CSC20", 
    "ZSTDF", "BCM", "ZLING", "MCM", "TANGELO", "ZMOLLY", "CRUSH", "LZJB",
    "BZIP2"
};

std::string CmZ::encoderName(unsigned iAlgorithm) {
    return encoderNames[iAlgorithm];
}

unsigned CmZ::encoder(unsigned iAlgorithm) {
    return bundle::encoders[iAlgorithm];
}

bool CmZ::isAlgorithmSupported(unsigned iAlgorithm) {
    if (std::binary_search(std::begin(bundle::unsupported), std::end(bundle::unsupported), iAlgorithm))
        return false;
    return true;
}

int CmZ::findAlgorithm(std::vector<char> bytes) {
    for (int i = 0; i < bytes.size() - 3; i ++) {
        if (bytes[i] == (char)0x00 && bytes[i+1] == (char)0x00 && bytes[i+2] == (char)0x00 && bytes[i+3] == (char)0x70){
            if ((unsigned int)bytes[i+4] < 24)
                return (int)bytes[i + 4];
        }
    }
    return INT_MAX;
}

bool CmZ::FileIsEmpty(std::string strFileName) {
    std::fstream file(strFileName,
        std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    file.seekp(0, std::ios_base::end);
    uint64_t size = file.tellp();
    file.close();
    return size < 8;
}
