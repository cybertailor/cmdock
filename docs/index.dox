/// \mainpage Main Page
///
/// \image html _static/logo-wide.svg "CmDock logo" height=200px
/// \image latex _static/logo.pdf "CmDock logo" width=8cm
///
/// \section main_intro Introduction
/// CmDock is a collection of optimisations, QoL and parallelism updates on a
/// fast and versatile **open-source** docking software that can be used
/// to dock **ligands** against **proteins** and **nucleic acids**. The software is capable
/// of high-throughput virtual screening (HTVS) campaigns and binding mode
/// prediction studies.
/// 
/// CmDock is named after Curie Marie and is dedicated to all women in science!
/// (Curium is a transuranic radioactive chemical element with the symbol Cm and atomic number 96.
/// This element of the actinide series was named after Marie and Pierre Curie,
/// both known for their research on radioactivity.)
///
/// CmDock is mainly written in C++, with accessory scripts and programs written in
/// C++, Python, Rust and Perl.
///
/// \section main_documentation Documentation
/// This documentation describes the code that makes up CmDock, a part of which
/// is the application programming interface (API).
///
/// This documentation does *not* describe the use of CmDock's command-line
/// interface (CLI), the preparation of the input files, or the post-processing
/// options available for the output files. For usage instructions, please see
/// the **Getting started guide**, the **User guide**, and the **Reference
/// manual**.
///
/// While intended for developers, this documentation also does *not* describe
/// how to format the code, customize the build system, use the version control
/// software, write the documentation and tests for the code, or test the code
/// changes using existing tests. These  topics are, among others, covered in
/// the **Developer guide**.
